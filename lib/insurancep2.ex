defmodule Insurancep2 do
  def count_by_bmi_cat() do
    File.read!("insurance.csv")
    |> String.split()
    |>tl()
    |> Enum.map(fn cadena -> String.split(cadena,",") end)
    |> Enum.map(fn row -> Enum.at(row, 2)  end)
    |> Enum.reduce(%{"Bajo peso" => 0, "Normal" => 0,"Sobre peso" => 0,"Obeso" => 0}, fn peso, count -> cond do
          elem(Float.parse(peso),0) < 18.5 -> Map.put(count, "Bajo peso", count["Bajo peso"] + 1)
          elem(Float.parse(peso),0) >= 18.5 and elem(Float.parse(peso),0) < 25 -> Map.put(count, "Normal", count["Normal"] + 1)
          elem(Float.parse(peso),0) >= 25 and elem(Float.parse(peso),0) < 30 -> Map.put(count, "Sobre peso", count["Sobre peso"] + 1)
          elem(Float.parse(peso),0) >= 30 -> Map.put(count, "Obeso", count["Obeso"] + 1)
          end end)
  end

  def relation_smoker_children() do
    File.read!("insurance.csv")
    |> String.split()
    |> tl()
    |> Enum.map(fn cadena -> String.split(cadena, ",") end)
    |> Enum.map(fn row -> [Enum.at(row, 3), Enum.at(row, 4)] end)
    |> Enum.group_by(fn x ->Enum.at(x,0)end)
    |> Enum.map(fn {hijos,lista} -> %{ hijos=>
      %{
        "Smoker"=> (Enum.filter(lista,fn x -> Enum.at(x,1)=="yes" end) |> Enum.count())/Enum.count(lista),
        "No Smoker"=> (Enum.filter(lista,fn x -> Enum.at(x,1)=="no" end) |> Enum.count())/Enum.count(lista),

    } }end )


  end



end
